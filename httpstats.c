#include <stdlib.h>
#include <errno.h>
#include <curl/curl.h>

#include "config.h"
#include "httpstats.h"

int list_add(list_t *list, char *payload)
{
    int rc = HSC_ERR;
    node_t *node = NULL;
    node_t *iterator = NULL;

    if ((NULL == list) || (NULL == payload)) {
        rc = EINVAL;
        goto _end;
    }

    node = calloc(1, sizeof(node_t));
    if (NULL == node) {
        rc = ENOMEM;
        goto _end;
    }
    node->payload = payload;

    if (NULL == list->head) {
        list->head = node;
    } else {
        iterator = list->head;
        while (NULL != iterator->next)
            iterator = iterator->next;
        iterator->next = node;
    }

    rc = HSC_OK;

_end:
    return rc;
}

void list_free(list_t *list)
{
    node_t *current = NULL;
    node_t *iterator = NULL;

    if (NULL == list)
        return;

    iterator = list->head;
    if (NULL == list->head)
        return;
    while (NULL != iterator) {
        current = iterator;
        iterator = iterator->next;
        free(current);
        current = NULL;
    }
}

void list_dump(list_t *list)
{
    node_t *iterator = NULL;

    if ((NULL == list) || (NULL == list->head))
        return;

    iterator = list->head;
    while (NULL != iterator) {
        printf("%s ", iterator->payload);
        iterator = iterator->next;
    }
    printf("\n");
}

#ifdef HAVE_CURL_CURL_H

http_stat_ctxt_t* httpstat_init(void)
{
    CURL *curl;
    http_stat_ctxt_t *ctxt = NULL;

    ctxt = calloc(1, sizeof(http_stat_ctxt_t));
    if (NULL == ctxt)
        goto _end;

    curl = curl_easy_init();
    if (NULL == curl) {
        free(ctxt);
        goto _end;
    }

    ctxt->handle = curl;

_end:
    return ctxt;
}

int httpstat_buid_request(http_stat_ctxt_t *ctxt,
        list_t *headers, const char *url)
{
    int rc = HSC_ERR;
    CURL *curl = NULL;
    struct curl_slist *list = NULL;
    FILE *stream = NULL;

    /* validate the context*/
    if ((NULL == ctxt) || (NULL == url)) {
        rc = EINVAL;
        goto _end;
    }
    
    curl = (CURL *)ctxt->handle;
    if (NULL == curl) {
        rc = EINVAL;
        goto _end;
    }
    
    /* add the headers to CURL internal data struct*/
    if (NULL != headers) {
        node_t *iterator = headers->head;
        while (NULL != iterator) {
            list = curl_slist_append(list, iterator->payload);
            iterator = iterator->next;
        }
        ctxt->priv = list;
    }

    /* build the request by setting the target URL and, eventually,
     * the list of headers*/
    curl_easy_setopt(curl, CURLOPT_URL, url);
    if (NULL != list) 
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, list);

    stream = fopen(".httpquery.log.", "w+");
    if (NULL != stream)
        curl_easy_setopt(curl, CURLOPT_WRITEDATA,(void *)stream); 
    
    rc = HSC_OK;

_end:
    return rc; 
}

int httpstat_trigger_request(http_stat_ctxt_t *ctxt)
{
    int rc = HSC_ERR;
    CURL *curl = NULL;

    /* validate the context*/
    if (NULL == ctxt) {
        rc = EINVAL;
        goto _end;
    }

    curl = (CURL *)ctxt->handle;
    if (NULL == curl) {
        rc = EINVAL;
        goto _end;
    }
        
    rc = curl_easy_perform(curl);

_end:
    return rc; 
}

int httpstat_read_info(http_stat_ctxt_t *ctxt,
        http_info_type_t stat, void *value)
{
    int rc;
    CURL *curl = NULL;

    /* validate the context*/
    if (NULL == ctxt) {
        rc = EINVAL;
        goto _end;
    }

    curl = (CURL *)ctxt->handle;
    if (NULL == curl) {
        rc = EINVAL;
        goto _end;
    }

    switch (stat) {
        case INFO_CONNECTION_TIME:
            rc = curl_easy_getinfo(curl,
                    CURLINFO_CONNECT_TIME, (double *)value);
            if (CURLE_OK != rc)
                goto _end;
            break;

        case INFO_START_TRANSFER_TIME:
            rc = curl_easy_getinfo(curl,
                    CURLINFO_STARTTRANSFER_TIME, (double *)value);
            if (CURLE_OK != rc)
                goto _end;
            break;

        case INFO_NAMELOOKUP_TIME:
            rc = curl_easy_getinfo(curl,
                    CURLINFO_NAMELOOKUP_TIME, (double *)value);
            if (CURLE_OK != rc)
                goto _end;
            break;

        case INFO_TOTAL_TIME:
            rc = curl_easy_getinfo(curl,
                    CURLINFO_TOTAL_TIME, (double *)value);
            if (CURLE_OK != rc)
                goto _end;
            break;
    
        case INFO_RESPONSE_CODE:
            rc = curl_easy_getinfo(curl,
                    CURLINFO_RESPONSE_CODE, (CURLcode *)value);
            if (CURLE_OK != rc)
                goto _end;
            break;

        case INFO_SERVER_IP:
            {
                char *ip = NULL;
                rc = curl_easy_getinfo(curl, CURLINFO_PRIMARY_IP, &ip);
                if (CURLE_OK != rc)
                    goto _end;

                sprintf(value, "%s", ip); 
            }
            break;
    }
    rc = HSC_OK;

_end:
    return rc; 
}

void httpstat_free(http_stat_ctxt_t *ctxt)
{
    CURL *curl = NULL;
    struct curl_slist *list = NULL;

    /* validate the context*/
    if (NULL == ctxt)
        return;

    curl = (CURL *)ctxt->handle;
    list = (struct curl_slist *)ctxt->priv;
    if (NULL == curl)
        return;
    
    if (NULL != list)
        curl_slist_free_all(list);

    curl_easy_cleanup(curl);
}

#else

http_stat_ctxt_t * httpstat_init(void)
{
    /*TO BE IMPLEMENTED*/

    return NULL; 
}

int httpstat_buid_request(http_stat_ctxt_t *ctxt,
        list_t *headers, const char *url)
{
    /*TO BE IMPLEMENTED*/

    return 0; 
}

int httpstat_send_request(http_stat_ctxt_t *ctxt)
{
    /*TO BE IMPLEMENTED*/

    return 0; 
}

int httpstat_read_info(http_stat_ctxt_t *ctxt,
        http_info_type_t stat, double *value)
{
    /*TO BE IMPLEMENTED*/

    return 0; 
}

void httpstat_free(http_stat_ctxt_t *ctxt)
{
    /*TO BE IMPLEMENTED*/

    return 0; 
}
#endif
