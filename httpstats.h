#ifndef HTTP_STATS_H_
#define HTTP_STATS_H_

#define HSC_OK  (0)
#define HSC_ERR (-1)

typedef struct node {
    char *payload;
    struct node *next;
} node_t;

typedef struct list {
    unsigned int num_nodes;
    node_t *head;
} list_t;

int list_add(list_t *list, char *payload);

void list_free(list_t *list);

void list_dump(list_t *list);

typedef struct http_stat_ctxt {
    void *handle;
    void *priv;
} http_stat_ctxt_t;

typedef enum {
    INFO_CONNECTION_TIME = 1,
    INFO_START_TRANSFER_TIME,
    INFO_NAMELOOKUP_TIME,
    INFO_TOTAL_TIME,
    INFO_RESPONSE_CODE,
    INFO_SERVER_IP,
    INFO_LAST
} http_info_type_t;

http_stat_ctxt_t* httpstat_init(void);

int httpstat_buid_request(http_stat_ctxt_t *ctxt,
        list_t *headers, const char *url);

int httpstat_trigger_request(http_stat_ctxt_t *ctxt);

int httpstat_read_info(http_stat_ctxt_t *ctxt,
        http_info_type_t stat, void *value);

void httpstat_free(http_stat_ctxt_t *ctxt);
#endif
