#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "httpstats.h"

static int cmpdouble(const void *d1, const void *d2)
{
    return ((*(const double *)d1) == (*(const double *)d2)) ? 0 : 
        (((*(const double *)d1) < (*(const double *)d2)) ? -1 : 1);
}

static void help(char *bin)
{
    printf("\n");
    printf("    Usage: %s [-H \"Header-name: Header-value\"] -n <integer> "
            "-u <URL>\n", bin);
    printf("\t -H \"Header-name: Header-value\" - specifies an extra HTTP "
            "header to be added to the HTTP request; can be used multiple times\n");
    printf("\t -n <integer> - specifies the number of HTTP requests to be triggered\n");
    printf("\t -u <URL> - specifies the target URL\n");
    printf("\n");
}

int main(int argc, char *args[])
{
    int rc = 0;
    char c;
    int i = 0;
    int times = 0;
    list_t headers = {0};
    const char *url = NULL;
    http_info_type_t hst;
    http_stat_ctxt_t *hsc = NULL;
    
    double *total_time = NULL;
    double *transfer_time = NULL;
    double *connection_time = NULL;
    double *namelookup_time = NULL;

    double total_time_median = 0.0;
    double transfer_time_median = 0.0;
    double connection_time_median = 0.0;
    double namelookup_time_median = 0.0;

    unsigned int code = 0;
    char ip[64] = {0};

    while((c = getopt(argc, args, "H:n:u:")) != -1) {
        switch (c)
        {
            case 'H':
                rc = list_add(&headers, optarg);
                if (HSC_ERR == rc) {
                    perror("err: ");
                    help(args[0]);
                    exit(rc);
                }
                break;
            case 'n':
                times = atoi(optarg);
                break;
            case 'u':
                url = strdup(optarg);
                break;
            case '?':
                fprintf( stderr, "Unrecognized option!\n" );
                break;
            default:
                help(args[0]);
                exit(1);
                break;
        }
    }

    if ((NULL == url) || (0 == times)) {
        help(args[0]);
        exit(2);
    }

    total_time = calloc(times, sizeof(double));
    if (NULL == total_time) {
        perror("err:");
        goto _end;
    }

    transfer_time = calloc(times, sizeof(double));
    if (NULL == transfer_time) {
        perror("err:");
        goto _end;
    }

    connection_time = calloc(times, sizeof(double));
    if (NULL == connection_time) {
        perror("err:");
        goto _end;
    }

    namelookup_time = calloc(times, sizeof(double));
    if (NULL == namelookup_time) {
        perror("err:");
        goto _end;
    }

#if 0 
    printf("Headers: ");
    list_dump(&headers);
    list_free(&headers);
    printf("Times: %d\n", times);
    printf("URL: %s\n", url);
#endif

    hsc = httpstat_init();
    httpstat_buid_request(hsc, &headers, url);
    for (i = 0; i < times; i++) {
        if (HSC_OK == httpstat_trigger_request(hsc)) {
            httpstat_read_info(hsc,
                    INFO_CONNECTION_TIME, &connection_time[i]);
            httpstat_read_info(hsc,
                    INFO_START_TRANSFER_TIME, &transfer_time[i]);
            httpstat_read_info(hsc,
                    INFO_NAMELOOKUP_TIME, &namelookup_time[i]);
            httpstat_read_info(hsc,
                    INFO_TOTAL_TIME, &total_time[i]);
            httpstat_read_info(hsc,
                    INFO_RESPONSE_CODE, &code);
        }
    }
            
    httpstat_read_info(hsc, INFO_SERVER_IP, ip);
    qsort(connection_time, times, sizeof(double), cmpdouble);
    qsort(transfer_time, times, sizeof(double), cmpdouble);
    qsort(namelookup_time, times, sizeof(double), cmpdouble);
    qsort(total_time, times, sizeof(double), cmpdouble);

    transfer_time_median = transfer_time[times/2];
    connection_time_median = connection_time[times/2];
    namelookup_time_median = namelookup_time[times/2];
    total_time_median = total_time[times/2];

#if 0
    printf("Transfer times:\n");
    for (i = 0; i < times; i++)
        printf("%.4f\n", transfer_time[i]);
    printf("\n");
    
    printf("Connection times:\n");
    for (i = 0; i < times; i++)
        printf("%.4f\n", connection_time[i]);
    printf("\n");
    
    printf("Namelookup times:\n");
    for (i = 0; i < times; i++)
        printf("%.4f\n", namelookup_time[i]);
    printf("\n");
    
    printf("Total times:\n");
    for (i = 0; i < times; i++)
        printf("%.4f\n", total_time[i]);
    printf("\n");
#endif

    printf("SKTEST;%s;%u;%.4f;%.4f;%.4f;%.4f\n", ip, code,
            namelookup_time_median, connection_time_median,
            transfer_time_median, total_time_median);
_end:
    if (NULL != total_time) {
        free(total_time);
        total_time = NULL;
    }
    if (NULL != transfer_time) {
        free(transfer_time);
        transfer_time = NULL;
    }
    if (NULL != connection_time) {
        free(connection_time);
        connection_time = NULL;
    }
    if (NULL != namelookup_time) {
        free(namelookup_time);
        namelookup_time = NULL;
    }
    if (NULL != hsc) {
        httpstat_free(hsc);
        hsc = NULL;
    }

    return 0;
}
